import sys

import discord
from discord.ext import commands

import codecs

client = discord.Client()

def loadInfo(path_to_file):
    file = codecs.open(path_to_file, 'r', encoding='utf-8').read()
    dic = {} # Создаем пустой словарь
    text = file.split('\r\n')
    for line in text: # Проходимся по каждой строчке
        key,value = line.split(': ')   # Разделяем каждую строку по двоеточии
        dic.update({key:value})       # Добавляем в словарь
    print(dic)                      # Вывод словаря на консоль
    return dic

async def getInfo(info):
    # print('getInfoCall')
    if info in bara_materials:
        return f'{info} содержит в себе:  {bara_materials[info]}'
    else:
        return 'Нет информации о данном веществе'


@client.event
async def on_ready():
    print('We have logged 2in as {0.user}'.format(client))


@client.event
async def on_message(message):
    print(f'on_messagecall {message.author}')
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')
    elif message.content.startswith('$bi'):
        st = message.content
        st = st[st.find(" ") + 1:]
        msg = await getInfo(st.lower())
        await message.channel.send(msg)
    elif message.content.startswith("я мол"):
        await message.reply(f"Ты молодец {message.author}!!!")
    elif (message.content.lower() == "shutdown"):
       await message.channel.send("Пока кожанные ублюдки...")
       await client.close()



# Загрузка токена сервера из файла. Файл должен быть в той же папке откуда запускаем main.py
tokenfile = open('./token')
token = tokenfile.read()

# Загрузка данных из разных источников
bara_materials = loadInfo('bara_materials')

# Запуск клиента
client.run(token)







